const webpack = require('webpack')
const path = require('path')
const { resolve } = require('path')
const nodeExternals = require('webpack-node-externals')

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    target: 'node',
    output: { path: __dirname, filename: 'bundle.js' },
    externals: [nodeExternals()],

    optimization: {
        minimize: false,
    },

    resolve: {
        alias: {
            '#common': resolve(__dirname, 'src/common/'),
            '#remote': resolve(__dirname, 'src/remote/'),
            '#logger': resolve(__dirname, 'src/logger/'),
            '#actions': resolve(__dirname, 'src/actions/'),
        },
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
        ],
    },
}
