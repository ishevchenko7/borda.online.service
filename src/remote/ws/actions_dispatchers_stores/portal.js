import _ from 'lodash'
import { v4 as uuidv4 } from 'uuid'

import PortalActionsDispatcher from '#actions/portal'
import SocketTransceiver from '#common/signal-ready/socket_transceiver'
import ActionsDispatchersStore from '#common/signal-ready/actions_dispatchers_store'

class PortalActionsDispatchersStore extends ActionsDispatchersStore {
    constructor() {
        super()
    }

    async add({ socket }) {
        const userId = uuidv4()

        super.add(
            userId,
            new PortalActionsDispatcher({
                transceiver: new SocketTransceiver({
                    socket,
                    settingsPath: 'portalSocketTransceiver',
                }),
            })
        )
    }
}

export default new PortalActionsDispatchersStore()
