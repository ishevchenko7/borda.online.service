import _ from 'lodash'
import { v4 as uuidv4 } from 'uuid'
import mongoose from 'mongoose'

import appActionsDispatchersStore from './app'
import SiteActionsDispatcher from '#actions/site'
import * as settingsStore from '#common/settings-store'
import SocketTransceiver from '#common/signal-ready/socket_transceiver'
import ActionsDispatchersStore from '#common/signal-ready/actions_dispatchers_store'

const Settings = mongoose.model('Settings')

class SiteActionsDispatchersStore extends ActionsDispatchersStore {
    constructor() {
        super()
    }

    async add({ socket, board: boardCode }) {
        const userId = uuidv4()
        const log = this.log.child({ userId, boardCode })

        const siteActionsDispatcher = new SiteActionsDispatcher({
            userId,
            boardCode,
            transceiver: new SocketTransceiver({ socket }),
        })

        try {
            await siteActionsDispatcher.remoteCall({
                type: 'initialize',
                args: { settings: await Settings.get('site') },
            })

            const appActionsDispatcher = appActionsDispatchersStore.get({
                id: boardCode,
            })
            if (appActionsDispatcher) {
                super.add(userId, siteActionsDispatcher)

                await appActionsDispatcher.remoteCall({
                    type: 'create-connection',
                    args: { user: userId },
                })

                await siteActionsDispatcher.remoteCall(
                    {
                        type: 'create-connection',
                        args: { user: userId },
                    },
                    settingsStore.get().createConnectionTimeout
                )

                socket.close()
                log.info('User successfully connected to board')
            } else {
                log.info('Board not connected')
                socket.close(4400)
            }
        } catch (error) {
            log.error(error)
            socket.close(4500)
        }
    }
}

export default new SiteActionsDispatchersStore()
