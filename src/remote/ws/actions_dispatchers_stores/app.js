import _ from 'lodash'
import mongoose from 'mongoose'

import pingPong from '../ping_pong'
import AppActionsDispatcher from '#actions/app'
import SocketTransceiver from '#common/signal-ready/socket_transceiver'
import ActionsDispatchersStore from '#common/signal-ready/actions_dispatchers_store'

const Board = mongoose.model('Board')
const Settings = mongoose.model('Settings')

class AppActionsDispatchersStore extends ActionsDispatchersStore {
    constructor() {
        super()
    }

    async authenticate({ login, password }) {
        const authenticated = await Board.authenticate({
            code: login,
            password,
        })

        if (!authenticated) {
            return `Authentication is fail`
        }
    }

    async add({ socket }) {
        let log = this.log
        const appActionsDispatcher = new AppActionsDispatcher({
            transceiver: new SocketTransceiver({ socket }),
        })

        try {
            const credentials = await appActionsDispatcher.remoteCall({
                type: 'get-credentials',
            })
            const boardCode = credentials.login
            log = log.child({ boardCode })

            const failMessage = this.get({ id: boardCode })
                ? `Double connection refused`
                : await this.authenticate(credentials)

            if (failMessage) {
                log.warn(failMessage)

                await appActionsDispatcher.remoteCall({
                    type: 'fail',
                    args: { failMessage },
                })
                // Forbidden
                socket.close(4403)
            } else {
                super.add(boardCode, appActionsDispatcher)

                await appActionsDispatcher.remoteCall({
                    type: 'initialize',
                    args: {
                        settings: await Settings.get('app'),
                        board: await Board.findOne({ code: boardCode }).select(
                            'code name -_id'
                        ),
                    },
                })

                pingPong(socket, log)
                log.info('Board successfully connected')
            }
        } catch (error) {
            log.error(error)
            // Internal error
            socket.close(4500)
        }
    }
}

export default new AppActionsDispatchersStore()
