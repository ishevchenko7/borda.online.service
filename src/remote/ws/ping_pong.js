import mongoose from 'mongoose'

import * as settingsStore from '#common/settings-store'

export default function (socket, log) {
    const { pongWaitTime, heartbeatTime } = settingsStore.get().webSocketServer

    let heartbeatTimer

    socket.on('close', () => {
        clearTimeout(heartbeatTimer)
        log.info('Stop heartbeat process')
    })

    socket.on('pong', () => {
        log.debug('Received ping')

        clearTimeout(heartbeatTimer)
        delayedPing()
    })

    function delayedPing() {
        heartbeatTimer = setTimeout(() => {
            socket.ping('')
            log.debug('Send ping')

            heartbeatTimer = setTimeout(() => {
                log.info('Broken connection detected')

                socket.terminate()
            }, pongWaitTime)
        }, heartbeatTime)
    }

    delayedPing()

    log.info('Start heartbeat process')
}
