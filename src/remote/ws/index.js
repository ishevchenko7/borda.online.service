import _ from 'lodash'
import { parse } from 'url'
import { Server } from 'ws'
import mongoose from 'mongoose'

import logger from '#logger'
import * as actionsDispatchersStores from './actions_dispatchers_stores'
import * as settingsStore from '#common/settings-store'

const log = logger.child({ script: '/index' })

function create() {
    const { port, host } = settingsStore.get().webSocketServer

    const server = new Server({
        port,
        host,
    })

    server.on('connection', async (socket, request) => {
        socket.on('error', (error) => log.error(error))

        const { source, ...rest } = parse(request.url, true).query
        await actionsDispatchersStores[source].add({ socket, ...rest })
    })

    log.info({ port }, 'Borda.online service is started')
}

export { create }
