import nodemailer from 'nodemailer'

import withSettings from '#common/settings-store/with_settings'

class MailTransport {
    constructor() {
        withSettings(this, 'mailTransport')
    }

    get mailTransport() {
        this.transport =
            this.transport || nodemailer.createTransport(this.settings)

        return this.transport
    }
}

export default new MailTransport().mailTransport
