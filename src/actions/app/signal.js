import siteActionsDispatchersStore from '#remote/ws/actions_dispatchers_stores/site'

export default async function (args) {
    const { user, signal } = args

    const siteActionsDispatcher = siteActionsDispatchersStore.get({ id: user })
    if (siteActionsDispatcher) {
        await siteActionsDispatcher.remoteCall({
            type: 'signal',
            args: { signal },
        })
    } else {
        return { error: 'Unknown user' }
    }
}
