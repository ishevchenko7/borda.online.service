import ActionsDispatcher from '#common/signal-ready/actions_dispatcher'

const actions = require.context('./', true, /^\.\/(?!index(\.js)?$)/)

class AppActionsDispatcher extends ActionsDispatcher {
    constructor(params) {
        super({ actions, ...params })
    }
}

export default AppActionsDispatcher
