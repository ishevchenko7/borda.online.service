import appActionsDispatchersStore from '#remote/ws/actions_dispatchers_stores/app'

export default async function (args, dispatcher) {
    const { signal } = args
    const { boardCode, userId } = dispatcher

    const appActionsDispatcher = appActionsDispatchersStore.get({
        id: boardCode,
    })
    if (appActionsDispatcher) {
        await appActionsDispatcher.remoteCall({
            type: 'signal',
            args: { user: userId, signal },
        })
    } else {
        return { error: 'Board not connected' }
    }
}
