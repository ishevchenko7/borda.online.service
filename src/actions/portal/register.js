import _ from 'lodash'
import { format } from 'util'
import fetch from 'node-fetch'
import mongoose from 'mongoose'

const Registration = mongoose.model('Registration')

export default async function (args, dispatcher) {
    const { boardCode, email, confirmationCode, password } = args

    if (_.isEqual(dispatcher.verifiedValues, { boardCode, email })) {
        delete dispatcher.verifiedValues

        await Registration.complete({
            boardCode,
            email,
            confirmationCode,
            password,
        })

        return { success: true }
    } else {
        return { error: 'Verified values is wrong' }
    }
}
