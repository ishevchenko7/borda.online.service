import _ from 'lodash'
import { format } from 'util'
import fetch from 'node-fetch'
import mongoose from 'mongoose'

import mail from '#remote/mail'
import * as settingsStore from '#common/settings-store'

const Registration = mongoose.model('Registration')

export default async function (args, dispatcher) {
    const { token, boardCode, email } = args

    const { recaptchaVerifyUrl } = settingsStore.get()
    const url = format(recaptchaVerifyUrl, token)

    const response = await fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
    })

    const { success } = await response.json()
    if (!success) {
        return {
            error: {
                name: 'recaptchaFail',
                message: 'Wrong reCaptcha. Hi robot)',
            },
        }
    }

    const confirmationCode = await Registration.start({ boardCode, email })
    const info = await mail.sendMail({
        from: '"borda.online" <admin@borda.online>',
        to: email,
        subject: 'Registration confirmation on borda.online',
        html: `Confirmation code for board https://${boardCode}.borda.online is: <b>${confirmationCode}</b>`,
    })

    if (!info?.messageId) {
        return { error: 'Confirmation email send error' }
    }

    dispatcher.verifiedValues = { boardCode, email }
    return { success }
}
