import mongoose from 'mongoose'

const Settings = mongoose.model('Settings')

export default async function () {
    const settings = await Settings.get('portal')
    return { settings }
}
