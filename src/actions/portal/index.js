import ActionsDispatcher from '#common/signal-ready/actions_dispatcher'

const actions = require.context('./', true, /^\.\/(?!index(\.js)?$)/)

class PortalActionsDispatcher extends ActionsDispatcher {
    constructor(params) {
        super({ actions, ...params })
    }

    afterSendDataHandler({ type, result }) {
        if (result?.error?.name === 'recaptchaFail') {
            this.transceiver.close()
        }
    }
}

export default PortalActionsDispatcher
