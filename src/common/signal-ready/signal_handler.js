const IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate
const SessionDescription =
    window.mozRTCSessionDescription || window.RTCSessionDescription

export default async function ({ transceiver, signal }) {
    const { rtcConnection, signalSender, log } = transceiver
    log.info({ signal }, 'Incoming signal')

    switch (signal.type) {
        case 'new_ice_candidate':
            const { candidate, sdpMLineIndex } = signal.data

            await rtcConnection.addIceCandidate(
                new IceCandidate({
                    sdpMLineIndex,
                    candidate,
                })
            )
            break

        case 'offer':
            const offerDescription = new SessionDescription(signal.data)
            await rtcConnection.setRemoteDescription(offerDescription)
            await rtcConnection.setLocalDescription(
                await rtcConnection.createAnswer()
            )

            const answer = {
                type: 'answer',
                data: rtcConnection.localDescription,
            }

            await signalSender(answer)
            log.info({ answer })
            break

        case 'answer':
            const answerDescription = new SessionDescription(signal.data)
            await rtcConnection.setRemoteDescription(answerDescription)
            break
    }
}
