import _ from 'lodash'
import isNode from 'detect-node'

import logger from '#logger'
import withSettings from '#common/settings-store/with_settings'
import withShortcutFields from './with_shortcut_fields'

export default class SocketTransceiver {
    constructor(params) {
        _.assign(this, params)
        withSettings(this, 'socketTransceiver')
        withShortcutFields(this, this.socket, ['addEventListener'])

        this.log = logger.child({ class: this.constructor.name })
        this.addEventListener('close', () => {
            if (this.closeTimer) {
                clearTimeout(this.closeTimer)
            }
        })
    }

    send(data) {
        this.socket.send(JSON.stringify(data))
        this.refreshCloseOnIdleTimer()
    }

    setMessageHandler(messageHandler) {
        this.addEventListener('message', ({ data }) => {
            messageHandler(JSON.parse(data))
            this.refreshCloseOnIdleTimer()
        })
    }

    refreshCloseOnIdleTimer() {
        const { closeIfIdleTimeout } = this.settings

        if (closeIfIdleTimeout) {
            if (isNode) {
                if (this.closeTimer) {
                    this.closeTimer.refresh()
                } else {
                    this.closeTimer = setTimeout(
                        () => this.socket.close(),
                        closeIfIdleTimeout
                    )
                }
            } else {
                this.log.warn(
                    'Setting closeIfIdleTimeout actual only for nodejs environment'
                )
            }
        }
    }
}
