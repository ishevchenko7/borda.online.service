import mongoose from 'mongoose'
import fs from 'fs'
import path from 'path'

const context = require.context('./models', true, /\.js$/)
context.keys().forEach(function (key) {
    context(key)
})

mongoose.Promise = global.Promise

function connect() {
    let options = {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    }

    mongoose.connect('mongodb://localhost/borda', options)
}

connect()
