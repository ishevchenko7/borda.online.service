import _ from 'lodash'
import bcrypt from 'bcrypt'
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const BoardSchema = new Schema({
    code: { type: String, unique: true },
    name: { type: String, default: '' },
    email: { type: String },
    hashedPassword: { type: String, default: '' },
})

BoardSchema.statics = {
    authenticate: async function ({ code, password }) {
        const board = await this.findOne({ code }).select('hashedPassword')

        if (board) {
            return (
                !_.isEmpty(password) &&
                (await bcrypt.compare(password, board.hashedPassword))
            )
        } else {
            return false
        }
    },
}

mongoose.model('Board', BoardSchema)
