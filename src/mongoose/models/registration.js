import _ from 'lodash'
import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import randomize from 'randomatic'

const Schema = mongoose.Schema

const RegistrationSchema = new Schema(
    {
        boardCode: { type: String },
        email: { type: String, default: '' },
        confirmationCode: { type: String, default: '' },
        confirmed: { type: Boolean, default: false },
    },
    { timestamps: true }
)

const Board = mongoose.model('Board')

RegistrationSchema.statics = {
    start: async function ({ boardCode, email }) {
        const session = await mongoose.startSession()
        session.startTransaction()
        try {
            const busy = await Board.findOne({ code: boardCode }).session(
                session
            )
            if (busy) {
                throw new Error('Board name is busy')
            }

            const associated = await Board.findOne({ email }).session(session)
            if (associated) {
                throw new Error(
                    'This e-mail already associated with some board'
                )
            }

            const confirmationCode = randomize('0', 6)
            await this.create(
                [
                    {
                        boardCode,
                        email,
                        confirmationCode,
                        confirmed: false,
                    },
                ],
                { session }
            )

            await session.commitTransaction()
            session.endSession()

            return confirmationCode
        } catch (error) {
            await session.abortTransaction()
            session.endSession()

            throw error
        }
    },

    complete: async function ({
        boardCode,
        email,
        confirmationCode,
        password,
    }) {
        const session = await mongoose.startSession()
        session.startTransaction()
        try {
            const updated = await this.findOneAndUpdate(
                {
                    boardCode,
                    email,
                    confirmationCode,
                    confirmed: false,
                },
                { confirmed: true },
                { session }
            )

            if (updated) {
                const hashedPassword = await bcrypt.hash(password, 8)
                await Board.create(
                    [{ code: boardCode, email, hashedPassword }],
                    { session }
                )
            } else {
                throw new Error('Incorrect confirmation code')
            }

            await session.commitTransaction()
            session.endSession()
        } catch (error) {
            await session.abortTransaction()
            session.endSession()

            throw error
        }
    },
}

mongoose.model('Registration', RegistrationSchema)
