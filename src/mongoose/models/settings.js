import _ from 'lodash'
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const SettingsSchema = new Schema({
    destinations: [{ type: String }],
    path: { type: String },
    value: { type: Object },
})

SettingsSchema.statics = {
    get: async function (destination) {
        const settings = await this.find({ destinations: destination })

        return _.reduce(
            settings,
            function (result, { path, value }) {
                return _.merge(result, path ? _.set({}, path, value) : value)
            },
            {}
        )
    },
}

mongoose.model('Settings', SettingsSchema)
