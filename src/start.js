import mongoose from 'mongoose'

import './mongoose'

import * as ws from '#remote/ws'
import * as settingsStore from '#common/settings-store'

const Settings = mongoose.model('Settings')

export default async function () {
    const settings = await Settings.get('service')
    settingsStore.set(settings)

    ws.create()
}
